﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EventHandler))]
public class PlaneCreator : MonoBehaviour
{
    private EventHandler m_EventHandle = null;
    private MeshRenderer m_MeshRenderer = null;

    private void Awake()
    {
        Setup();

        m_EventHandle = this.GetComponent<EventHandler>();
        m_EventHandle.OnTrackingFound += Show;
        m_EventHandle.OnTrackingLost += Hide;
    }
    private void OnDestroy()
    {
        m_EventHandle.OnTrackingFound -= Show;
        m_EventHandle.OnTrackingLost -= Hide;
    }
    private void Setup()
    {
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.transform.parent = this.transform;
        plane.transform.localEulerAngles = new Vector3(0, 180f, 0);
        plane.transform.localScale = new Vector3(0.15f, 0.15f, 0.15f);

        m_MeshRenderer = plane.GetComponent<MeshRenderer>();
        m_MeshRenderer.material = Resources.Load<Material>("M_Scene");

        Hide();
    }
    private void Show()
    {
        m_MeshRenderer.enabled = true;
    }
    private void Hide()
    {
        m_MeshRenderer.enabled = false;
    }
}
