﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Linq;

public class TargetManager : MonoBehaviour
{
    [SerializeField] string m_DatabaseName = "";

    private List<TrackableBehaviour> m_AllTargets = new List<TrackableBehaviour>();

    private void Awake()
    {
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDestroy()
    {
        VuforiaARController.Instance.UnregisterVuforiaStartedCallback(OnVuforiaStarted);
    }
    private void OnVuforiaStarted()
    {
        //Load Database
        LoadDatabase(m_DatabaseName);

        //Get Trackable objects
        m_AllTargets = GetTargets();

        //Setup targets
        SetupTargets(m_AllTargets);
    }
    private void LoadDatabase(string setName)
    {
        //Stop the object tracker before loading database
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        objectTracker.Stop();

        //Load the dataset
        if (DataSet.Exists(setName))
        {
            DataSet dataset = objectTracker.CreateDataSet();
            dataset.Load(setName);
            objectTracker.ActivateDataSet(dataset);
        }
        objectTracker.Start();
    }

    private List<TrackableBehaviour> GetTargets()
    {
        List<TrackableBehaviour> allTrackables = new List<TrackableBehaviour>();
        allTrackables = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours().ToList();
        return allTrackables;
    }
    private void SetupTargets(List<TrackableBehaviour> allTargets)
    {
        foreach (TrackableBehaviour target in allTargets)
        {
            //Parent to the Target Manager
            target.transform.parent = this.transform;

            //Rename to easily find when needed
            target.gameObject.name = target.TrackableName;

            //Add functionality
            target.gameObject.AddComponent<PlaneCreator>();

            //Debug
            Debug.Log("Trackable object: " + target.name + " created");
        }
    }
}
